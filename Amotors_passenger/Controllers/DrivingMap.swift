//
//  DrivingMap.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 09.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation
import SVProgressHUD
import PusherSwift
import WebKit
class DrivingMap: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,CLLocationManagerDelegate,WKNavigationDelegate,PusherDelegate{
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menu_btn: UIButton!
    var locationManager = CLLocationManager()
    @IBOutlet weak var status_lb: UILabel!
    var traivelhistory: [TravelDetail] = []
    var pusher: Pusher! = nil
    var channel : PusherChannel! = nil
    var location_timer = Timer()
    var  channel_update_location : PusherChannel! = nil
    let  userid = UserDefaults.standard.string(forKey: "user_id")!
    var order_id  = UserDefaults.standard.string(forKey: "order_id")!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMap()
        getOrderInfo()
        createPusher()
        self.location_timer = Timer.scheduledTimer(timeInterval:TimeInterval(10), target: self, selector: (#selector(sendLocationToServer)), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    func getAcces () {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    func configUI(){
        //self.hideKeyboardWhenTappedAround()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.register(UINib(nibName: "mark_infoTableViewCell", bundle: nil), forCellReuseIdentifier: "mark_cell")
        tableView.register(UINib(nibName: "carNumberTableViewCell", bundle: nil), forCellReuseIdentifier: "car_cell")
        tableView.register(UINib(nibName: "DriverInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "driver_cell")
        self.view.addSubview(cancel_btn)
        CreateConstraints()
        getAcces()
        locationManager.startUpdatingLocation()
    }
    let cancel_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Отменить заказ",for: .normal)
        // view.backgroundColor = UIColor(red: 63/255, green: 135/255, blue: 245/255, alpha: 1.0)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(cancel_btn_clicked), for: .touchUpInside)
        return view
    }()
    func CreateConstraints()  {
        // constraints for forgot button
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
            cancel_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        }
        else {
            cancel_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        }
        cancel_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        cancel_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        cancel_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0){
                return 100
            }
        else {
            return 44
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
          let cell = tableView.dequeueReusableCell(withIdentifier: "mark_cell", for: indexPath) as! mark_infoTableViewCell
            let car_color = hexStringToUIColor(hex: traivelhistory[0].colorHex)
            cell.car_mark_lb.text = traivelhistory[0].brand
            cell.color_name_lb.text = traivelhistory[0].color
      //      cell.car_number_lb.text = traivelhistory[0].
            cell.color_view.backgroundColor = car_color
                return cell
            
        }
        else if (indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "car_cell", for: indexPath) as! carNumberTableViewCell
               cell.car_number_lb.text = traivelhistory[0].registration_number
            return cell
        }
        else {
              let cell = tableView.dequeueReusableCell(withIdentifier: "driver_cell", for: indexPath) as! DriverInfoTableViewCell
              cell.driver_name_lb.text = traivelhistory[0].driverName + " " + traivelhistory[0].driverSurname + " " + traivelhistory[0].driverPatronymic
              cell.call_btn.addTarget(self, action: #selector(call_toDriver), for: .touchUpInside)
                return cell
            }
    }
    @objc func call_toDriver(_ sender: AnyObject?) {
        guard let number = URL(string: "tel://" + traivelhistory[0].driverPhone) else { return }
        UIApplication.shared.open(number)
    }
    private func loadMap() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestAlwaysAuthorization()
                locationManager.startUpdatingLocation()
            case .authorizedAlways, .authorizedWhenInUse:
                var myURL = URL(string: "")
                let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
                let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
                myURL =  URL(string: "\(BaseMessage.baseUrl)/map-passanger-driver?passanger_id=\(userid)&city_id=1&method=init&longitude=\(myLongitude)&latitude=\(myLatitude)&order_id=\(order_id))")
                print("asffasasf",myURL)
                let myRequest = URLRequest(url: myURL!)
                webView = WKWebView(frame: webView.frame)
                view.addSubview(webView)
                webView.navigationDelegate = self
                webView.load(myRequest)
                CreateConstraintsWebview()
                configUI()
            }
        } else {
            SVProgressHUD.showError(withStatus:BaseMessage.locationdisabled)
            print("Location services are not enabled")
        }
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //createPusher()
    }
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let user = "dev@amotors.kz"
        let password = "123123"
        let credential = URLCredential(user: user, password: password, persistence: URLCredential.Persistence.forSession)
        challenge.sender?.use(credential, for: challenge)
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
    }
    func CreateConstraintsWebview(){
        //constatraints for webview
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        webView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0).isActive = true
    }
    func getOrderInfo(){
        SVProgressHUD.show()
        let url = "\(BaseMessage.baseUrl)/orders/\(order_id)?with[]=driver&with[]=company&with[]=driver.car&with[]=passanger&with[]=driver.car.brand&with[]=driver.car.color"
        print("afafs",url)
        let credentialData = "dev@amotors.kz:123123".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        Alamofire.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate().responseJSON { response in
                guard response.result.isSuccess else {
                    print("Ошибка при запросе данных \(String(describing: response.result.error))")
                    return
                }
                SVProgressHUD.dismiss()
                let json = try! JSON(data: (response.data)!)
                for (_,subJson):(String, JSON) in json {
                    let new = TravelDetail(json: subJson)
                    self.traivelhistory.append(new)
                }
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
        }
        
    }
    @objc func cancel_btn_clicked(_ sender: AnyObject?) {
       
    }
    @IBAction func menu_btn(_ sender: Any) {
          revealViewController()?.revealLeftView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // hex to rgb color
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func createPusher() {
        let pusherClientOptions = PusherClientOptions(authMethod: .inline(secret: "f288a464d3766c8a7fd6"),host: .cluster("ap2"))
        pusher = Pusher(key: "7ea5886145e8af112d5f", options: pusherClientOptions)
        pusher.delegate = self
        channel_update_location = pusher.subscribe(channelName: "private-passanger-location")
        pusher.connect()
        // sendFirstLocation()
    }
    @objc func sendLocationToServer(timer: Timer){
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "passanger_id":userid,
            "location": savedData,
            ]
        print("without_order_working",jsonObject)
        channel_update_location.trigger(eventName: "client-location-updated", data:jsonObject)
    }
    func sendFirstLocation(){
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "passanger_id":userid,
            "location": savedData,
            ]
        print("without_order_working",jsonObject)
        channel_update_location.trigger(eventName: "client-location-updated", data:jsonObject)
    }
   
}
