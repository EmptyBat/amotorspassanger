//
//  MainViewController.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 08.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation
import SVProgressHUD
import PusherSwift
import WebKit
class MainViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,CLLocationManagerDelegate,WKNavigationDelegate,PusherDelegate{
    @IBOutlet weak var search_tf: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menu_btn: UIButton!
    @IBOutlet weak var search_btn: UIButton!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var time_btn: UIButton!
    @IBOutlet weak var tv_height: NSLayoutConstraint!
    var locationManager = CLLocationManager()
    let  userid = UserDefaults.standard.string(forKey: "user_id")!
    var pusher: Pusher! = nil
    var channel : PusherChannel! = nil
    var section_count = 1
    static let two_gisKey = "ruhfmm6698"
    var selected = true
    var searchModelData: [SearchModel] = []
    var districtModelData: [SearchModel] = []
    var opened = false
    var location_timer = Timer()
    var  channel_update_location : PusherChannel! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        getAcces()
        loadMap()
        self.location_timer = Timer.scheduledTimer(timeInterval:TimeInterval(10), target: self, selector: (#selector(sendLocationToServer)), userInfo: nil, repeats: true)
        
    }
    func createPusher() {
        let pusherClientOptions = PusherClientOptions(authMethod: .inline(secret: "f288a464d3766c8a7fd6"),host: .cluster("ap2"))
        pusher = Pusher(key: "7ea5886145e8af112d5f", options: pusherClientOptions)
        pusher.delegate = self
        let channel_local = pusher.subscribe(channelName: "passanger-location")
        let _ = channel_local.bind(eventName: "client-location-updated", callback: { (data: Any?) -> Void in if let data = data as? [String: AnyObject]  {
            let json = JSON(data)
            let new = DraggedModel(json: json)
            self.search_tf.text = new.title
            self.done_btn.isHidden = false
            let savedData: NSDictionary = ["lon": new.lon,
                                           "lat": new.lat]
            let jsons = JSON(["point":"\(savedData)", "caption": new.title])
            let newSearchModel = SearchModel(json :jsons)
            self.districtModelData[0] = newSearchModel
            }  })
         channel_update_location = pusher.subscribe(channelName: "private-passanger-location")
        pusher.connect()
       // sendFirstLocation()
    }
    @objc func sendLocationToServer(timer: Timer){
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "passanger_id":userid,
            "location": savedData,
            ]
        print("without_order_working",jsonObject)
        channel_update_location.trigger(eventName: "client-location-updated", data:jsonObject)
    }
    func sendFirstLocation(){
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "passanger_id":userid,
            "location": savedData,
            ]
        print("without_order_working",jsonObject)
        channel_update_location.trigger(eventName: "client-location-updated", data:jsonObject)
    }
    func getAcces () {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    func configUI(){
        //self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.search_tf.delegate = self
        self.search_tf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.register(UINib(nibName: "district_nameTableViewCell", bundle: nil), forCellReuseIdentifier: "district_cell")
        tableView.register(UINib(nibName: "district_addedTableViewCell", bundle: nil), forCellReuseIdentifier: "additional_cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.addSubview(done_btn)
        CreateConstraints()
        self.done_btn.isHidden = true
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(title: "Указать на карте", style: .plain, target: self, action: #selector(cancelNumberPad)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        search_tf.inputAccessoryView = numberToolbar
        self.tableView.reloadData()
        locationManager.startUpdatingLocation()
    }
   @objc func cancelNumberPad() {
        dismissKeyboard()
        section_count = 1
        tv_height.constant = 307
         tableView.isScrollEnabled = false
         UIView.animate(withDuration: 0.3) {
         self.view.layoutIfNeeded()
         }
        searchModelData.removeAll()
        self.tableView.reloadData()
    }
    @objc func doneWithNumberPad() {
     dismissKeyboard()
    }
    @objc func keyboardWillShow(sender: Notification) {
        if (districtModelData.count != 7){
        //done_btn.addSubview(self.view)
        opened = true
        let screenSize = UIScreen.main.bounds
        tableView.isScrollEnabled = true
        tv_height.constant = screenSize.height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
           // self.done_btn.removeFromSuperview()
            //self.view.addSubview(done_btn)
           // self.CreateConstraints()
        }
        else {
            opened = false
            self.dismissKeyboard()
        }
    }
   // сағындым
    @objc func keyboardWillHide(sender: Notification) {
      /*  tv_height.constant = 310
        tableView.isScrollEnabled = false
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }*/
         searchModelData.removeAll()
        self.tableView.reloadData()
        
    }
    func CreateConstraintsWebview(){
        //constatraints for webview
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        webView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0).isActive = true
    }
    func searchData(string :String) {
        if (string.count <= 3){
        searchModelData.removeAll()
        let escapedString = "lang=ru&q=\(string)&key=\(MainViewController.two_gisKey)&region_id=67".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
          if let url_decoded = escapedString {
        let url = "\(BaseMessage.twoGisUrl)\(String(describing: url_decoded))"
        Alamofire.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:nil)
            .validate().responseJSON { response in
                guard response.result.isSuccess else {
                    print("Ошибка при запросе данных \(String(describing: response.result.error))")
                    return
                }
                let json = try! JSON(data: (response.data)!)
                for (_,subJson):(String, JSON) in json["result"]["items"] {
                    let new = SearchModel(json :subJson)
                    self.searchModelData.append(new)
                }
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
        }
        
    }
        }
    }
    //люблю сильно сильно я тебя тоже
    func numberOfSections(in tableView: UITableView) -> Int {
        return section_count
    }
    func getPassangerLocation(){
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let escapedString = "longitude=\(myLongitude)&latitude=\(myLatitude)&passanger_id=\(userid)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        if let url_decoded = escapedString {
            let url = "\(BaseMessage.crmAmotorsUrl)\(String(describing: url_decoded))"
            Alamofire.request(url,
                              method: .get,
                              parameters: nil,
                              encoding: URLEncoding.default,
                              headers:nil)
                .validate().responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Ошибка при запросе данных \(String(describing: response.result.error))")
                        return
                    }
                    let json = try! JSON(data: (response.data)!)
                    let new = DraggedModel(json: json)
                    self.search_tf.text = new.title
                    self.done_btn.isHidden = false
                    print("affasfas",new.lon,new.lat)
                    let savedData: NSDictionary = ["lon": new.lon,
                        "lat": new.lat]
                    let jsons = JSON(["point":savedData, "caption": new.title])
                    print("123",savedData)
                    let newSearchModel = SearchModel(json :jsons)
                      print("1233",jsons)
                    self.districtModelData.append(newSearchModel)
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0 ){
        return searchModelData.count + 1
        }
        else if (section == 1){
          return districtModelData.count
        }
        else {
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0){
        if (searchModelData.count == indexPath.row) {
        return 50
    }
        else {
        return 60
       }
        }
        else {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0){
        if (searchModelData.count == indexPath.row){
            let cell:UITableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: "fact_cell") as UITableViewCell?)!
            return cell
        }
        else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "district_cell", for: indexPath) as! district_nameTableViewCell
        cell.name_lb.text = searchModelData[indexPath.row].title
        return cell
    }
        }
        else if (districtModelData.count == indexPath.row + 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "additional_cell", for: indexPath) as! district_addedTableViewCell
            cell.district_lb.text = districtModelData[indexPath.row].title
            cell.icon_img.image = UIImage(named:"marketb_icon")
            return cell
        }
        else {
            if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "additional_cell", for: indexPath) as! district_addedTableViewCell
            cell.district_lb.text = districtModelData[indexPath.row].title
            cell.icon_img.image = UIImage(named:"location_api")
                 return cell
            }
            else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "additional_cell", for: indexPath) as! district_addedTableViewCell
                cell.district_lb.text = districtModelData[indexPath.row].title
                cell.icon_img.image = UIImage(named:"intermediateb_icon")
                 return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (districtModelData.count != 7)&&(indexPath.section == 0){
            districtModelData.append(searchModelData[indexPath.row])
            section_count = 2
            searchModelData.removeAll()
            search_tf.text = ""
            self.dismissKeyboard()
            self.done_btn.isHidden = false
            tableView.reloadData()
        }
        
    }
    @IBAction func time_btn(_ sender: Any) {
    }
    @IBAction func search_btn(_ sender: Any) {
    }
    @IBAction func menu_btn(_ sender: Any) {
          revealViewController()?.revealLeftView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Fact_btn(_ sender: Any) {
        create_order_by_fact()
    }
    @objc func textFieldDidChange(textField: UITextField){
        if (textField.text!.count != 0 ){
            if let searchText = textField.text{
            searchData(string: searchText)
            }
        }
        print("Text changed")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BY_FACT" {
            let destination: LoadingViewController = segue.destination as! LoadingViewController
            destination.waitString = "ожидайте"
        }
        else if (segue.identifier == "next"){
            let destination: SetTimeController = segue.destination as! SetTimeController
            destination.districtModelData = districtModelData
            
        }
    }
    private func loadMap() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestAlwaysAuthorization()
                locationManager.startUpdatingLocation()
            case .authorizedAlways, .authorizedWhenInUse:
                var myURL = URL(string: "")
                let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
                let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
                myURL =  URL(string: "\(BaseMessage.baseUrl)/map-passanger?passanger_id=\(userid)&city_id=1&method=init&longitude=\(myLongitude)&latitude=\(myLatitude)")
                print("asffasasf",myURL)
                let myRequest = URLRequest(url: myURL!)
                webView = WKWebView(frame: webView.frame)
                view.addSubview(webView)
                webView.navigationDelegate = self
                webView.load(myRequest)
                CreateConstraintsWebview()
                configUI()
                getPassangerLocation()
            }
        } else {
            SVProgressHUD.showError(withStatus:BaseMessage.locationdisabled)
            print("Location services are not enabled")
        }
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          createPusher()
    }
    func CreateConstraints()  {
            // constraints for forgot button
            if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
                done_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
            }
            else {
                done_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
            }
            done_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
            done_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
            done_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    let done_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Куда поедем?",for: .normal)
        // view.backgroundColor = UIColor(red: 63/255, green: 135/255, blue: 245/255, alpha: 1.0)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(done_btn_clicked), for: .touchUpInside)
        return view
    }()
    @objc func done_btn_clicked(_ sender: AnyObject?) {
        if (opened == false){
        let screenSize = UIScreen.main.bounds
        tableView.isScrollEnabled = true
        tv_height.constant = screenSize.height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        section_count = 2
        searchModelData.removeAll()
        search_tf.text = ""
        self.dismissKeyboard()
        self.done_btn.isHidden = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        self.search_tf.becomeFirstResponder()
        opened = true
        }
        else {
            if (districtModelData.count >= 2){
             performSegue(withIdentifier: "next", sender: nil)
            }
            else {
            SVProgressHUD.showError(withStatus: BaseMessage.selectMoreDistricts)
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
       /* let channel_local = pusher.subscribe(channelName: "private-driver-location")
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "status_id":"4",
            "driver_id":(UserDefaults.standard.string(forKey: "user_id")),
            "location": savedData,
            "car_id":(UserDefaults.standard.string(forKey: "car_id")),
            ]
        print("asfasf",jsonObject)
        channel_local.trigger(eventName: "client-location-updated", data:jsonObject)*/
    }
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let user = "dev@amotors.kz"
        let password = "123123"
        let credential = URLCredential(user: user, password: password, persistence: URLCredential.Persistence.forSession)
        challenge.sender?.use(credential, for: challenge)
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
    }
    func create_order_by_fact() {
        let now = Date()
        
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateString = formatter.string(from: now)
        SVProgressHUD.show()
        let url = "\(BaseMessage.baseUrl)/orders"
        let credentialData = "dev@amotors.kz:123123".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        let params = ["city_id": "1","car_type_id":"1","passanger_id":userid,"order_at":dateString,"data_from[name]" :districtModelData[0].title,"data_from[longitude]":districtModelData[0].lon,"data_from[latitude]":districtModelData[0].lat,"is_drive_by_fact":"1"]
        print("asfsafaf",params)
        Alamofire.request(url,
                          method: .post,
                          parameters: params,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate().responseJSON { response in
                guard response.result.isSuccess else {
                    print("Ошибка при запросе данных \(String(describing: response.result.error))")
                    return
                }
                self.performSegue(withIdentifier: "BY_FACT", sender: nil)
                SVProgressHUD.dismiss()
                //let json = try! JSON(data: (response.data)!)
                
                
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
