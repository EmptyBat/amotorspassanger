//
//  SetTimeController.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 09.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
class SetTimeController: UIViewController {

    var order: [OrderInfo] = []
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var sw_own: UISwitch!
    var order_at = ""
    var districtModelData: [SearchModel] = []
    var additional_districts : [String : String] = [:]
    let  userid = UserDefaults.standard.string(forKey: "user_id")!
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        CreateConstraints()
        // Do any additional setup after loading the view.
    }
    @IBAction func datePickerChanged(_ sender: Any) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "yyyy-MM-dd HH:MM:ss"
        order_at = dateFormatter.string(from: datePicker.date)
        print(order_at)
    }
    @IBAction func menu_btn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
   //   revealViewController()?.revealLeftView()
    }
    @IBOutlet weak var own_sw: UISwitch!
    func configUI(){
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        self.view.addSubview(done_btn)
        getTime()
    }

    let done_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Заказать машину",for: .normal)
        // view.backgroundColor = UIColor(red: 63/255, green: 135/255, blue: 245/255, alpha: 1.0)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(done_btn_clicked), for: .touchUpInside)
        return view
    }()
    func loadData() {
        SVProgressHUD.show()
        let url = "\(BaseMessage.baseUrl)/orders"
        let credentialData = "dev@amotors.kz:123123".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        let params = ["city_id": "1","car_type_id":"1","passanger_id":"1","order_at":order_at,]
        Alamofire.request(url,
                          method: .post,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate().responseJSON { response in
                guard response.result.isSuccess else {
                    print("Ошибка при запросе данных \(String(describing: response.result.error))")
                    return
                }
                SVProgressHUD.dismiss()
                //let json = try! JSON(data: (response.data)!)
            
            
        }
        
    }
    func create_order_by_fact() {
        SVProgressHUD.show()
        let url = "\(BaseMessage.baseUrl)/orders"
        let credentialData = "dev@amotors.kz:123123".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        let params = ["city_id": "1","car_type_id":"1","passanger_id":"1","order_at":order_at,]
        Alamofire.request(url,
                          method: .post,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate().responseJSON { response in
                guard response.result.isSuccess else {
                    print("Ошибка при запросе данных \(String(describing: response.result.error))")
                    return
                }
                SVProgressHUD.dismiss()
                //let json = try! JSON(data: (response.data)!)
                
                
        }
    }
    func getTime(){
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "yyyy-MM-dd HH:MM:ss"
        order_at = dateFormatter.string(from: datePicker.date)
    }
    func create_order() {
        SVProgressHUD.show()
        let url = "\(BaseMessage.baseUrl)/orders"
        let credentialData = "dev@amotors.kz:123123".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        var sw_int = 1
        if (sw_own.isOn  == false){
            sw_int = 0
        }
        else {
            sw_int = 1
        }
        let params = ["city_id": "1","car_type_id":"1","passanger_id":userid,"order_at":order_at,"data_from[name]" :districtModelData[0].title,"data_from[longitude]":districtModelData[0].lon,"data_from[latitude]":districtModelData[0].lat,"data_to[name]":districtModelData[1].title,"data_to[longitude]":districtModelData[1].lon,"data_to[latitude]":districtModelData[1].lat,"is_own_expense":"\(sw_int)"]
        print("asfsfafas",params)
        print("url",url)
        Alamofire.request(url,
                          method: .post,
                          parameters: params,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate().responseJSON { response in
                guard response.result.isSuccess else {
                    print("Ошибка при запросе данных \(String(describing: response.result.error))")
                    return
                }
                SVProgressHUD.dismiss()
                let json = try! JSON(data: (response.data)!)
                for (_,subJson):(String, JSON) in json {
                    let new = OrderInfo(json: subJson)
                    self.order.append(new)
                }
                UserDefaults.standard.set(self.order[0].order_id, forKey: "order_id")
                print("success",json)
                self.performSegue(withIdentifier: "drive", sender: nil)
                
        }
    }
    func create_order_with_additional() {
        getAdress()
        SVProgressHUD.show()
        let url = "\(BaseMessage.baseUrl)/orders"
        let credentialData = "dev@amotors.kz:123123".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        print("asfsfafas",additional_districts)
        Alamofire.request(url,
                          method: .post,
                          parameters: additional_districts,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate().responseJSON { response in
                guard response.result.isSuccess else {
                    print("Ошибка при запросе данных \(String(describing: response.result.error))")
                    return
                }
                SVProgressHUD.dismiss()
                let json = try! JSON(data: (response.data)!)
                for (_,subJson):(String, JSON) in json {
                    let new = OrderInfo(json: subJson)
                    self.order.append(new)
                }
                 UserDefaults.standard.set(self.order[0].order_id, forKey: "order_id")
                print("success",json)
                self.performSegue(withIdentifier: "drive", sender: nil)
                
        }
    }
    func CreateConstraints()  {
        // constraints for forgot button
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
            done_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        }
        else {
            done_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        }
        done_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        done_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        done_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    @objc func done_btn_clicked(_ sender: AnyObject?) {
        print("asfsafaf",districtModelData.count)
        if (districtModelData.count == 2){
        create_order()
        }
        else if (districtModelData.count > 2){
        create_order_with_additional()
        }
        else {
            performSegue(withIdentifier: "drive", sender: nil)
        }
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getAdress(){
        var sw_int = 0
        if (sw_own.isOn  == false){
            sw_int = 0
        }
        else {
            sw_int = 1
        }
        for index in 1...districtModelData.count-2 {
            if (index == 1){
                additional_districts = ["data_additional[\(index)][name]":districtModelData[index].title,"data_additional[\(index)][longitude]":districtModelData[index].lon,"data_additional[\(index)][latitude]":districtModelData[index].lat,"city_id": "1","car_type_id":"1","passanger_id":userid,"order_at":order_at,"data_from[name]" :districtModelData[0].title,"data_from[longitude]":districtModelData[0].lon,"data_from[latitude]":districtModelData[0].lat,"data_to[name]":districtModelData[districtModelData.count-1].title,"data_to[longitude]":districtModelData[districtModelData.count-1].lon,"data_to[latitude]":districtModelData[districtModelData.count-1].lat,"is_own_expense":"\(sw_int)"]
            }
            else if (index == 2) {
                additional_districts = ["data_additional[\(1)][name]":districtModelData[1].title,"data_additional[\(1)][longitude]":districtModelData[1].lon,"data_additional[\(1)][latitude]":districtModelData[1].lat,"data_additional[\(2)][name]":districtModelData[2].title,"data_additional[\(2)][longitude]":districtModelData[2].lon,"data_additional[\(2)][latitude]":districtModelData[2].lat,"city_id": "1","car_type_id":"1","passanger_id":userid,"order_at":order_at,"data_from[name]" :districtModelData[0].title,"data_from[longitude]":districtModelData[0].lon,"data_from[latitude]":districtModelData[0].lat,"data_to[name]":districtModelData[districtModelData.count-1].title,"data_to[longitude]":districtModelData[districtModelData.count-1].lon,"data_to[latitude]":districtModelData[districtModelData.count-1].lat,"is_own_expense":"\(sw_int)"]
            }
            else if (index == 3) {
                additional_districts = ["data_additional[\(1)][name]":districtModelData[1].title,"data_additional[\(1)][longitude]":districtModelData[1].lon,"data_additional[\(1)][latitude]":districtModelData[1].lat,"data_additional[\(2)][name]":districtModelData[2].title,"data_additional[\(2)][longitude]":districtModelData[2].lon,"data_additional[\(2)][latitude]":districtModelData[2].lat,"data_additional[\(3)][name]":districtModelData[3].title,"data_additional[\(3)][longitude]":districtModelData[3].lon,"data_additional[\(3)][latitude]":districtModelData[3].lat,"city_id": "1","car_type_id":"1","passanger_id":userid,"order_at":order_at,"data_from[name]" :districtModelData[0].title,"data_from[longitude]":districtModelData[0].lon,"data_from[latitude]":districtModelData[0].lat,"data_to[name]":districtModelData[districtModelData.count-1].title,"data_to[longitude]":districtModelData[districtModelData.count-1].lon,"data_to[latitude]":districtModelData[districtModelData.count-1].lat,"is_own_expense":"\(sw_int)"]
            }
            else if (index == 4) {
                additional_districts = ["data_additional[\(1)][name]":districtModelData[1].title,"data_additional[\(1)][longitude]":districtModelData[1].lon,"data_additional[\(1)][latitude]":districtModelData[1].lat,"data_additional[\(2)][name]":districtModelData[2].title,"data_additional[\(2)][longitude]":districtModelData[2].lon,"data_additional[\(2)][latitude]":districtModelData[2].lat,"data_additional[\(3)][name]":districtModelData[3].title,"data_additional[\(3)][longitude]":districtModelData[3].lon,"data_additional[\(3)][latitude]":districtModelData[3].lat,"data_additional[\(4)][name]":districtModelData[4].title,"data_additional[\(4)][longitude]":districtModelData[4].lon,"data_additional[\(4)][latitude]":districtModelData[4].lat,"city_id": "1","car_type_id":"1","passanger_id":userid,"order_at":order_at,"data_from[name]" :districtModelData[0].title,"data_from[longitude]":districtModelData[0].lon,"data_from[latitude]":districtModelData[0].lat,"data_to[name]":districtModelData[districtModelData.count-1].title,"data_to[longitude]":districtModelData[districtModelData.count-1].lon,"data_to[latitude]":districtModelData[districtModelData.count-1].lat,"is_own_expense":"\(sw_int)"]
            }
            else if (index == 5) {
                additional_districts = ["data_additional[\(1)][name]":districtModelData[1].title,"data_additional[\(1)][longitude]":districtModelData[1].lon,"data_additional[\(1)][latitude]":districtModelData[1].lat,"data_additional[\(2)][name]":districtModelData[2].title,"data_additional[\(2)][longitude]":districtModelData[2].lon,"data_additional[\(2)][latitude]":districtModelData[2].lat,"data_additional[\(3)][name]":districtModelData[3].title,"data_additional[\(3)][longitude]":districtModelData[3].lon,"data_additional[\(3)][latitude]":districtModelData[3].lat,"data_additional[\(4)][name]":districtModelData[4].title,"data_additional[\(4)][longitude]":districtModelData[4].lon,"data_additional[\(4)][latitude]":districtModelData[4].lat  ,"data_additional[\(5)][name]":districtModelData[5].title,"data_additional[\(5)][longitude]":districtModelData[5].lon,"data_additional[\(5)][latitude]":districtModelData[5].lat,"city_id": "1","car_type_id":"1","passanger_id":userid,"order_at":order_at,"data_from[name]" :districtModelData[0].title,"data_from[longitude]":districtModelData[0].lon,"data_from[latitude]":districtModelData[0].lat,"data_to[name]":districtModelData[districtModelData.count-1].title,"data_to[longitude]":districtModelData[districtModelData.count-1].lon,"data_to[latitude]":districtModelData[districtModelData.count-1].lat,"is_own_expense":"\(sw_int)"]
                
            }
        }
        print("test count",additional_districts)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
