//
//  WaitController.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 09.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class WaitController: UIViewController {
    var status = true
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(label_loading)
        view.addSubview(background_loader)
        view.addSubview(cancel_btn)
        CreateConstraints()
        self.animateLoadingLabel()
    }
    let label_loading: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "ожидайте"
        view.textAlignment = .center
        return view
    }()
    @objc func animateLoadingLabel()
    {
        if status
        {
            if label_loading.text == "ожидайте..."
            {
                label_loading.text = "ожидайте"
            }
            else
            {
                label_loading.text = (label_loading.text!+".")
            }
            
            perform(#selector(animateLoadingLabel), with: nil, afterDelay: 1)
        }
    }
    let background_loader: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "loader_background@3x")!
        view.image = image
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    let cancel_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "cancel_icon")!
        view.setImage(image, for: .normal)
        view.addTarget(self, action: #selector(dismissToRootController), for: .touchUpInside)
        return view
    }()
    func CreateConstraints()  {
        // constraints for background_img
        background_loader.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        background_loader.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        background_loader.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        background_loader.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -self.view.frame.height/2.5).isActive = true
        background_loader.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        // constraints for label loading
        label_loading.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        label_loading.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        label_loading.topAnchor.constraint(equalTo: background_loader.bottomAnchor, constant: 100).isActive = true
        label_loading.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        label_loading.heightAnchor.constraint(equalToConstant: 44).isActive = true
        // constraints for cancel button
        cancel_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        cancel_btn.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        cancel_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        cancel_btn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
    }
    
}
