//
//  DraggedModel.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 11/18/18.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import SwiftyJSON
import UIKit
struct DraggedModel{
    
    let title: String
    let lat: String
    let lon: String
    
    init(json: JSON) {
        self.lat = json["latitude"].stringValue
        self.lon = json["longitude"].stringValue
        self.title = json["name"].stringValue
    }
    
}
