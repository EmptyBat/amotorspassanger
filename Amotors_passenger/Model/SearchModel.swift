//
//  SearchModel.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 09.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import SwiftyJSON
import UIKit
struct SearchModel{
    
    let title: String
    let lat: String
    let lon: String
    
    init(json: JSON) {
        self.lat = json["point"]["lat"].stringValue
        self.lon = json["point"]["lon"].stringValue
        self.title = json["caption"].stringValue
    }
    
}
