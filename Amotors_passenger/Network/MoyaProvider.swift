//
//  MoyaProvider.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 04.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import Foundation
import UIKit
import Moya
import Alamofire
import SVProgressHUD
import SwiftyJSON

struct NetworkManager {
    
    static let provider = MoyaProvider<APIEndpoint>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
    static func makeRequest(target: APIEndpoint, success successCallback: @escaping (JSON) -> Void) {
        //   SVProgressHUD.show()
        provider.request(target) { (result) in
            
            switch result {
            case .success(let response):
                let wrong_json = try! JSON(data: response.data)
                let str = wrong_json["data"].arrayObject
                let convert = "\(str)"
                if (wrong_json["data"]["phone"] == "These credentials do not match our records."){
                    SVProgressHUD.showError(withStatus:BaseMessage.passwordError)
                }
                if (convert == "Optional([Поле confirmation code указано неверно])"){
                    SVProgressHUD.showError(withStatus:"Код подтверждения неверный")
                }
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    let json = try! JSON(data: response.data)
                    SVProgressHUD.dismiss()
                    if response.statusCode == 200 {
                        successCallback(json)
                    } else {
                        //  SVProgressHUD.showError(withStatus:BaseMessage.passwordError)
                    }
                    print ("working",json)
                    if (json["code"] == 302)
                    {
                        SVProgressHUD.showError(withStatus:BaseMessage.passwordError)
                    }
                }
            case .failure(let error):
                SVProgressHUD.showError(withStatus: error.errorDescription)
            }
        }
    }
}
