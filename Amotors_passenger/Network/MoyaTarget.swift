//
//  MoyaTarget.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 04.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import Foundation
import Moya
import Alamofire

let tmProvider = MoyaProvider<APIEndpoint>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
var pathtourl = ""
public enum APIEndpoint {
    
    // Auth
    case signIn(params: [String: Any])
    case getNewPassword(phone:String)
    case password_recovery(params: [String: Any])
    
    // order
    case getPassengerInfo(id: Int)
    case getNewOrder(order_id: Int)
    case changeDriverStatus(status_id: Int, user_id : Int,longitude : String , latitude: String)
    
    //Order history
    case history(limit: String,driver_id : Int)
    case orderDetails(order_id: String,params:[String: Any])
    
    //Orders pull
    case ordersPull(city_id : Int)
    
    // push notifications
    case setDeviceToken(params:[String: Any],entity_id : String,device_token : String)
    case reset_badge_count(token: String)
    
    //search
    case searchLocation(name : String)
    
    //info for create order
    case car_types()
    
    //create order
    case createOrderByFact(city_id : String,car_type_id : String,passanger_id : String,is_drive_by_fact : String,order_at : String,data_fromName : String ,data_fromLongitude : String,data_fromLatitude : String)
    
}
extension APIEndpoint: TargetType {
    
    public var baseURL: URL { return URL(string: "http://188.166.117.195/api/v1")! }
    
    public var path: String {
        switch self {
        // Auth
        case .signIn:
            return "/passanger/login"
        case .password_recovery:
            return "/passanger/password"
        case .getNewPassword:
            return "/passanger/password"
        // search
        case .getPassengerInfo(let user_id):
            return "/passangers/"+"\(user_id)"
        // order
        case .getPassengerInfo(let user_id):
            return "/passangers/"+"\(user_id)"
        case .getNewOrder(let order_id):
            return "/orders/"+"\(order_id)"
        case .changeDriverStatus(let status_id,let user_id,let longitude,let latitude):
            return "/drivers/\(user_id)/change_status"
            
        // push notifications
        case .setDeviceToken:
            return "/device_tokens"
        case .reset_badge_count:
            return "/device_tokens/reset_badge_count"
        // Order history
        case .history(let limit,let driver_id):
            return "/drivers/\(driver_id)/history"
        case .orderDetails(let order_id,let body):
            return "orders/\(order_id)"
        // Orders pull
        case .ordersPull(let city_id):
            return "orders_pull"
        // search location
        case .searchLocation(let name):
             return "/locations"
        // car types
        case.car_types():
            return "/car_types"
            
        // create order infos
        case.car_types():
            return "/car_types"
        // create order
        case.createOrderByFact(let city_id , let car_type_id,let passanger_id , let is_drive_by_fact,let order_at,let data_fromName,let data_fromLongitude,let data_fromLatitude):

            return "/orders/"
        
            
        default:
            return ""
        }
    }
    public var method: Moya.Method {
        switch self {
        case .signIn,.password_recovery,.setDeviceToken,.changeDriverStatus, .reset_badge_count,.getNewPassword:
            return .post
        default:
            return .get
        }
    }
    public var parameters: [String: Any]? {
        
        var params:[String: Any] = [:] // for post params
        var query:[String: Any] = [:] // for get params
        switch self {
            
        case .signIn(let dict):
            params["phone"] = dict["phone"]
            params["password"] = dict["password"]
            break
        case .getNewPassword(let phone):
            params["phone"] = phone
            break
        // order
        case .getPassengerInfo(let user_id):
            query["query"] = user_id
            pathtourl = "\(user_id)"
            break
        case .password_recovery(let dict):
            params["phone"] = dict["phone"]
            break
        case .changeDriverStatus(let dict,let dict2,let longitude,let latitude):
            params["status_id"] = dict
            params["longitude"] = longitude
            params["latitude"] = latitude
            break
        // push notifications
        case .setDeviceToken(let dict ,let entity_id,let device_token):
            params["token"] = dict["token"]
            params["entity_id"] = entity_id
            params["device_token"] = device_token
            break
        case .reset_badge_count(let token):
            params["token"] = token
            break
        // order history
        case .history(let limit,let driver_id):
            query["limit"] = limit
        case .orderDetails(let order_id,let body):
            query = body
        case .ordersPull(let city_id):
            query["city_id"] = "\(city_id)"
        case .searchLocation(let name):
            query["like[title]"] = "\(name)"
        default:
            break
        }
        
        // for query - params["query"], POST - params["body"]
        params["query"] = query
        return params
    }
    
    /* public var parameterEncoding: ParameterEncoding {
     switch self {
     case .getUserComments:
     return CompositeJsonEncoding()
     default:
     return CompositeEncoding()
     }
     
     }
     */
    
    public var sampleData: Data {
        return "Default sample data".data(using: String.Encoding.utf8)!
    }
    public var task: Task {
        switch self {
            /*  case .uploadAvatar(let data):
             let imgData = MultipartFormData(provider: MultipartFormData.FormDataProvider.data(data), name: "file", fileName: "userAva.jpg", mimeType: "image/jpeg")
             let multipartData = [imgData]
             return .uploadMultipart(multipartData)
             case .uploadReportImage(let data):
             let imgData = MultipartFormData(provider: .data(data), name: "file", fileName: "userAva.jpg", mimeType: "image/jpeg")
             let multipartData = [imgData]
             return .uploadMultipart(multipartData) */
        case .password_recovery:
            if let phone = (parameters!["phone"]) {
                return .requestParameters(parameters: ["phone": "\(phone)"], encoding: URLEncoding.httpBody)
            }
            else {
                return .requestPlain
            }
        case .signIn:
            if let phone = (parameters!["phone"]), let password = (parameters!["password"]) {
                return .requestParameters(parameters: ["phone": "\(phone)", "password": "\(password)"], encoding: URLEncoding.httpBody)
            }
            else {
                return .requestPlain
            }
        case .getNewPassword:
            if let phone = (parameters!["phone"]) {
                return .requestParameters(parameters: ["phone": "\(phone)"], encoding: URLEncoding.httpBody)
            }
            else {
                return .requestPlain
            }
        case .setDeviceToken:
            if let token = (parameters!["token"]), let entity_id = (parameters!["entity_id"]), let device_token = (parameters!["device_token"]) {
                print ("mac os id",entity_id)
                let originalString = "App\\Models\\Passanger"
                return .requestParameters(parameters: ["token": "\(device_token)", "entity_id": "\(entity_id)", "type_id": "\("1")","entity_type": "\(originalString)"], encoding: JSONEncoding.default)
            }
            else {
                return .requestPlain
            }
        case .reset_badge_count:
            if let token = (parameters!["token"]) {
                return .requestParameters(parameters: ["token": "\(token)"], encoding: URLEncoding.httpBody)
            }
            else {
                return .requestPlain
            }
        case .changeDriverStatus:
            if let status_id = (parameters!["status_id"]), let longitude = (parameters!["longitude"]), let latitude = (parameters!["latitude"]) {
                return .requestParameters(parameters: ["status_id": "\(status_id)","longitude": "\(longitude)","latitude": "\(latitude)"], encoding: URLEncoding.httpBody)
            }
            else {
                return .requestPlain
            }
        case .history:
            if let limit = (parameters!["query"]) {
                return .requestParameters(parameters: limit as! [String : Any], encoding: URLEncoding.queryString)
            }
            else {
                return .requestPlain
            }
            break
        case .orderDetails:
            if let body = (parameters!["query"]) {
                return .requestParameters(parameters: body as! [String : Any], encoding: CompositeEncoding())
            }
            else {
                return .requestPlain
            }
        case .ordersPull:
            if let city_id = (parameters!["query"]) {
                return .requestParameters(parameters: city_id as! [String : Any], encoding: URLEncoding.queryString)
            }
            else {
                return .requestPlain
            }
        case .searchLocation:
            if let city_id = (parameters!["query"]) {
                return .requestParameters(parameters: city_id as! [String : Any], encoding: URLEncoding.queryString)
            }
            else {
                return .requestPlain
            }
            break
        default:
            return .requestPlain
            
        }
    }
    
    public var validate: Bool {
        return true
    }
    
    public var headers: [String : String]? {
        var params:[String: String] = [:] // for post params
        params["Authorization"] = "Basic ZGV2QGFtb3RvcnMua3o6MTIzMTIz"
        return params
    }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString + pathtourl
    
}

struct CompositeEncoding: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        guard let parameters = parameters else {
            return try urlRequest.asURLRequest()
        }
        let queryParameters = (parameters["query"] as! Parameters)
        var queryRequest = try URLEncoding(destination: .queryString).encode(urlRequest, with: queryParameters)
        if let body = parameters["body"] {
            let bodyParameters = (body as! Parameters)
            
            let req = try URLEncoding(destination: .queryString).encode(urlRequest, with: bodyParameters)
            if let url = req.url, let query = url.query {
                queryRequest.httpBody = query.data(using: .utf8)
            }
            return queryRequest
        } else {
            return queryRequest
            
        }
    }
}
struct CompositeJsonEncoding: ParameterEncoding {
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        guard let parameters = parameters else {
            return try urlRequest.asURLRequest()
        }
        let queryParameters = (parameters["query"] as! Parameters)
        var queryRequest = try URLEncoding(destination: .queryString).encode(urlRequest, with: queryParameters)
        if let body = parameters["body"] {
            let bodyParameters = (body as! Parameters)
            do {
                let data = try JSONSerialization.data(withJSONObject: bodyParameters, options: [])
                
                if queryRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                    queryRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                
                queryRequest.httpBody = data
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return queryRequest
    }
}
private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

