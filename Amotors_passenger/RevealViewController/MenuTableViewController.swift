//
//  MenuTableViewController.swift
//  Example4Swift
//
//  Created by Patrick BODET on 09/08/2016.
//  Copyright © 2016 iDevelopper. All rights reserved.
//

import UIKit
import PBRevealViewController
import SDWebImage
class MenuTableViewController: UITableViewController {
    var listData = ["История поездок", "Связь с диспетчером", "Выйти"]
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count+1
    }
   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if (indexPath.row == 0){
        return 157
    }
        return 60
        }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
       let cell = tableView.dequeueReusableCell(withIdentifier: "profile_cell", for: indexPath) as! ProfileTableViewCell
        cell.username_lb.text = UserDefaults.standard.string(forKey: "name")
        cell.phone_lb.text = UserDefaults.standard.string(forKey: "phone")
        cell.avatar_img.sd_setImage(with: URL(string:UserDefaults.standard.string(forKey: "photo")!), placeholderImage: UIImage(named: ""))
        return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideBar_cell", for: indexPath) as! SideBarDataTableViewController
            cell.title_lb.text = listData[indexPath.row-1]
            if (indexPath.row == listData.count-1){
                cell.call_btn.isHidden = false
                
            }
            return cell
            
        }
    }
    func configUI() {
        tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "profile_cell")
              tableView.register(UINib(nibName: "SideBarDataTableViewController", bundle: nil), forCellReuseIdentifier: "SideBar_cell")
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
          if (indexPath.row == 2) {
            if UserDefaults.standard.bool(forKey: "driverIsBusy") == false {
              AuthManager.shared.logout()
            }
        }
        if (indexPath.row == 1){
            guard let number = URL(string: "tel://" + "+77022789898") else { return }
            UIApplication.shared.open(number)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    // MARK: - Table view delegate
    }
}
