//
//  BaseMessage.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 04.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

struct BaseMessage {
    static var ImageUrl = "http://188.166.117.195"
    static var baseUrl = "http://188.166.117.195/api/v1"
    static var twoGisUrl = "https://catalog.api.2gis.ru/2.0/suggest/endpoint/list?"
    static var crmAmotorsUrl = "http://crm.amotors.kz/dragged?"
    // static let appColor = UIColor(red:0.49, green:0.19, blue:0.89, alpha:1.0)
    static let appColor = UIColor.white
    static let textcolor = UIColor.init(red: 0/255, green: 152/255, blue: 244/255, alpha: 1)
    static let passwordError = "Неправильный логин или пароль"
    static let parsingError = "Ошибка в обработке данных"
    static let fillError = "Заполните всю необходимую информацию"
    static let commentAdded = "Комментарий отправлен на модерацию"
    static let wrongcode = "Неверный код"
    static let locationdisabled = "Включите геолокацию"
    static let orderSucces = "Заказ завершен"
    static let orderTime = "Время заказа еще не подошло"
    static let orderRemoved = "Заказ Отменен"
    static let selectMoreDistricts = "Вы не выбрали адрес"
}
