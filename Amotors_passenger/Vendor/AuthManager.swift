//
//  AuthManager.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 04.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SDWebImage
struct AuthManager {
    static let shared: AuthManager = {
        let instance = AuthManager()
        return instance
    }()
    func logout(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LOGOUT")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
        UserDefaults.standard.removeObject(forKey: "status_sw")
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "driverIsBusy")
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        UserDefaults.standard.synchronize()
    }
    
    func login(_ user_id: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
        UserDefaults.standard.setValue(user_id, forKey: "user_id")
        UserDefaults.standard.synchronize()
    }
    func check_login() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let userid = UserDefaults.standard.string(forKey: "user_id") {
            if UserDefaults.standard.bool(forKey: "PassangerIsBusy") == true {
                let vc = storyboard.instantiateViewController(withIdentifier: "MAIN")
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window!.rootViewController = vc
            }
            else {
                let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window!.rootViewController = vc
            }
        } else {
            let vc = storyboard.instantiateViewController(withIdentifier: "LOGOUT")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = vc
        }
    }
}
