//
//  DriverInfoTableViewCell.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 25.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class DriverInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var driver_name_lb: UILabel!
    @IBOutlet weak var call_btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
