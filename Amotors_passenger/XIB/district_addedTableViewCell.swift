//
//  district_addedTableViewCell.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 24.09.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class district_addedTableViewCell: UITableViewCell {
    @IBOutlet weak var icon_btn: UIButton!
    @IBOutlet weak var icon_img: UIImageView!
    @IBOutlet weak var district_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
