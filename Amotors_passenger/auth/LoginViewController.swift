//
//  LoginViewController.swift
//  Amotors_passenger
//
//  Created by Almas Abdrasilov on 09.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import InputMask
import SVProgressHUD
import CoreLocation
class LoginViewController: UIViewController , MaskedTextFieldDelegateListener,CLLocationManagerDelegate{
    var maskedDelegate: MaskedTextFieldDelegate?
    var isValidPhone = false
    var phoneNumber = ""
    var password_left: NSLayoutConstraint?
    var password_right: NSLayoutConstraint?
    var passwort_top: NSLayoutConstraint?
    var passwort_height: NSLayoutConstraint?
    let viewModel = AuthViewModel()
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubObjects()
        CreateConstraints()
        configureUI()
        getAcces()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureUI() {
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        maskedDelegate = MaskedTextFieldDelegate(format: "{+7} ([000]) [000] [00] [00]")
        maskedDelegate?.listener = self
        phone_tf.delegate = maskedDelegate
    }
    func addSubObjects() {
        view.addSubview(scrollView)
        scrollView.addSubview(background_img)
        scrollView.addSubview(profile_img)
        scrollView.addSubview(sign_btn)
        scrollView.addSubview(phone_tf)
        scrollView.addSubview(password_tf)
        scrollView.addSubview(forgot_btn)
        scrollView.addSubview(phone_img)
        scrollView.addSubview(password_img)
    }
    @objc func keyboardWillShow(sender: Notification) {
        //  self.scrollView.frame.origin.y = -100
    }
    
    @objc func keyboardWillHide(sender: Notification) {
        // self.scrollView.frame.origin.y = 0
    }
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        phoneNumber = value
        isValidPhone = complete
    }
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let background_img: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "login_background")!
        view.image = image
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    let profile_img: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "people_icon")!
        view.image = image
        return view
    }()
    let phone_img: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "phone_icon")!
        view.image = image
        return view
    }()
    let password_img: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "password_icon")!
        view.image = image
        return view
    }()
    let sign_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("ВОЙТИ",for: .normal)
        // view.backgroundColor = UIColor(red: 63/255, green: 135/255, blue: 245/255, alpha: 1.0)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(sign_btn_clicked), for: .touchUpInside)
        return view
    }()
    let forgot_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Напомнить пароль?",for: .normal)
        view.layer.cornerRadius = 8
        view.setTitleColor(UIColor.black, for: .normal)
        view.addTarget(self, action: #selector(re_pass_clicked), for: .touchUpInside)
        return view
    }()
    let phone_tf: SkyFloatingLabelTextFieldWithIcon = {
        let view = SkyFloatingLabelTextFieldWithIcon()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = NSAttributedString(string: "Телефон",
                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        view.textColor = UIColor.white
        view.lineColor = UIColor.white
        view.selectedLineColor = UIColor.lightGray
        view.selectedTitleColor = UIColor.lightGray
        view.keyboardType = UIKeyboardType.phonePad
        return view
    }()
    let password_tf: SkyFloatingLabelTextFieldWithIcon = {
        let view = SkyFloatingLabelTextFieldWithIcon()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = NSAttributedString(string: "Пароль",
                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        view.isSecureTextEntry = true
        view.textColor = UIColor.white
        view.lineColor = UIColor.white
        view.selectedLineColor = UIColor.lightGray
        view.selectedTitleColor = UIColor.lightGray
        view.keyboardType = UIKeyboardType.phonePad
        view.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        return view
    }()
    func getAcces () {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        if (textField.text!.count == 6){
            viewModel.signIn(phone: (phone_tf.text?.replacingOccurrences(of: " ", with: ""))!, password: password_tf.text!)
            performSegue(withIdentifier: "loading", sender: nil)
        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        phone_tf.text = "+7"
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func CreateConstraints()  {
        //constraints for scrollView
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        // constraints for background_img
        background_img.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 0).isActive = true
        background_img.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: 0).isActive = true
        background_img.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        background_img.heightAnchor.constraint(equalTo: scrollView.heightAnchor, multiplier: 1*0.50).isActive = true
        background_img.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        // constraints for background_img
        profile_img.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
            profile_img.topAnchor.constraint(equalTo: background_img.centerYAnchor, constant:0).isActive = true
            passwort_top = password_tf.bottomAnchor.constraint(equalTo: background_img.bottomAnchor, constant: -20)
            
        }
        else{
            profile_img.topAnchor.constraint(equalTo: background_img.centerYAnchor, constant:30).isActive = true
            passwort_top = password_tf.bottomAnchor.constraint(equalTo: background_img.bottomAnchor, constant: -20)
            
        }
        profile_img.heightAnchor.constraint(equalToConstant: 60).isActive = true
        profile_img.widthAnchor.constraint(equalToConstant: 60).isActive = true
        // constraints for phone textfield
        phone_tf.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 40).isActive = true
        phone_tf.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -40).isActive = true
        phone_tf.topAnchor.constraint(equalTo: profile_img.bottomAnchor, constant: 20).isActive = true
        phone_tf.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // constraints for password textfield
        password_left = password_tf.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: phone_tf.frame.width)
        password_right = password_tf.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -view.frame.width)
        password_tf.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // constraints for sign button
        passwort_top?.isActive = true
        password_right?.isActive = true
        password_left?.isActive = true
        password_tf.isHidden = true
        sign_btn.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 20).isActive = true
        sign_btn.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -20).isActive = true
        sign_btn.topAnchor.constraint(equalTo: background_img.bottomAnchor, constant: 60).isActive = true
        sign_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        // constraints for forgon button
        forgot_btn.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 20).isActive = true
        forgot_btn.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -20).isActive = true
        forgot_btn.topAnchor.constraint(equalTo: sign_btn.bottomAnchor, constant: 20).isActive = true
        forgot_btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // constraints for phone image
        phone_img.rightAnchor.constraint(equalTo: phone_tf.leftAnchor, constant: +15).isActive = true
        phone_img.centerYAnchor.constraint(equalTo: phone_tf.centerYAnchor, constant: +5).isActive = true
        phone_img.heightAnchor.constraint(equalToConstant: 19).isActive = true
        phone_img.widthAnchor.constraint(equalToConstant: 13).isActive = true
        // constraints for password image
        password_img.rightAnchor.constraint(equalTo: password_tf.leftAnchor, constant: +15).isActive = true
        password_img.centerYAnchor.constraint(equalTo:  password_tf.centerYAnchor, constant: +5).isActive = true
        password_img.heightAnchor.constraint(equalToConstant: 18).isActive = true
        password_img.widthAnchor.constraint(equalToConstant: 14).isActive = true
        password_img.isHidden = true
        forgot_btn.isHidden = true
    }
    func showpassword(){
        password_left?.constant = 40
        password_right?.constant = -40
        passwort_top?.constant = -30
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
            passwort_top?.constant = -20
        }
        else{
            passwort_top?.constant = -30
        }
        password_img.isHidden = false
        password_tf.isHidden = false
        background_img.heightAnchor.constraint(equalTo: scrollView.heightAnchor, multiplier: 1*0.65).isActive = true
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            self.view.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            self.password_tf.becomeFirstResponder()
        })
    }
    @objc func sign_btn_clicked(_ sender: AnyObject?) {
        view.endEditing(true)
        //    self.tv_height.constant = self.tableView.frame.height-120
        if  let phone = phone_tf.text, !phone.isEmpty ,let password = password_tf.text, password.isEmpty {
            viewModel.getNewPassword(phone: phone.replacingOccurrences(of: " ", with: ""))
            showpassword()
        }
        else if let phone = phone_tf.text, !phone.isEmpty ,let password = password_tf.text, !password.isEmpty{
            viewModel.signIn(phone: phone.replacingOccurrences(of: " ", with: ""), password: password)
            performSegue(withIdentifier: "loading", sender: nil)
        }
        else if let phone = phone_tf.text, phone.isEmpty ,let password = password_tf.text,password.isEmpty{
            SVProgressHUD.showError(withStatus: BaseMessage.fillError)
        }
    }
    @objc func re_pass_clicked(_ sender: AnyObject?) {
        view.endEditing(true)
        performSegue(withIdentifier: "RePass", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loading" {
            let destination: LoadingViewController = segue.destination as! LoadingViewController
            destination.waitString = "авторизация"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textField(textField: UITextField,
                   shouldChangeCharactersInRange range: NSRange,
                   replacementString string: String) -> Bool{
        
        let oldText: NSString = textField.text! as NSString
        let newText: NSString = oldText.replacingCharacters(in: range, with: string) as NSString
        
        let count = newText.length
        if count <= 5 {
            print("\(count)")
            return true
        } else {
            return false
        }
    }
}
